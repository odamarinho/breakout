extends Area2D

export var speed = 200

onready var h_width = $Sprite.texture.get_size().x / 2

func _process(delta):
	if Input.is_action_pressed("ui_left"):
		if position.x > h_width:
			position.x -= speed * delta
	
	elif Input.is_action_pressed("ui_right"):
		if position.x < get_viewport().size.x - h_width:
			position.x += speed * delta

func _on_Plank_area_entered(area):
	if area.is_in_group("ball"):
		var diff = abs(area.position.x - position.x)
		area.direction.y *= -1
		area.direction.x = (diff / h_width) * sign(area.direction.x)


extends Area2D

export var speed = 325

var direction = Vector2(1, 1)

onready var h_width = $Sprite.texture.get_size().x / 2
onready var h_height = $Sprite.texture.get_size().y / 2

func _process(delta):
	position.x += speed * direction.x * delta
	position.y += speed * direction.y* delta
	
	if position.x < h_width or position.x > get_viewport().size.x - h_width:
		direction.x *= -1
	
	if position.y < h_height or position.y > get_viewport().size.y - h_height:
		direction.y *= -1


func _on_Ball_area_entered(area):
	if area.is_in_group("block"):
		GameController.destroy_block(area)
		direction.y *= -1

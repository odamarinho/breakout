extends Control

var points_hud
export var n_blocks = 0

func _ready():
	points_hud = $hud_points
	GameController.connect("destroy_block", self, "_on_destroy_block")

func _on_destroy_block():
	points_hud.text = str(GameController.points)
	
	n_blocks -= 1
	
	if n_blocks == 0:
		GameController.next_stage()


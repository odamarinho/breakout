extends Node

var points = 0

signal destroy_block

func _ready():
	VisualServer.set_default_clear_color(Color(0, 0, 0, 1))

func destroy_block(var block):
	points += 5
	block.queue_free()
	emit_signal("destroy_block")

func next_stage():
	print("PASSOU!!")
	reload_scene() #<-retirar essa linha na implementação com mais fases
	#Carregar a Nova Fase

func reload_scene():
	get_tree().reload_current_scene()
	points = 0